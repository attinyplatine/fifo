/*
 * fifo.c
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
  *      Hier befindet sich das eigentliche Programm. Es wird auf die verschiedenen Programmteile zugegriffen und alles zu einem funktionierenden und interagierenden Programm vereint. Es stehen mehrere Funktionen zur Verfügung, welche zu unterschiedlichen Zeitpunkten aufgerufen werden.
 */
#include <stdbool.h>
#include <stdio.h>
#include "fifo.h"
#include "../io.h"

struct fifo {
	volatile char data[FIFOSIZE];
	volatile int readPointer;
	volatile int writePointer;
}
		tx_buffer= {{}, 0, 0},
		rx_buffer= {{}, 0, 0};
// Initialisierung, weil alle Programmteile über einen solchen verfügen, der Vollständigkeit halber implementiert, bei der fifo wäre dies nicht nötig.
void fifosetup(void){
}

bool checkTX(void){
	bool ret =(tx_buffer.writePointer + 1 == tx_buffer.readPointer ) ||( tx_buffer.readPointer == 0 && tx_buffer.writePointer + 1 == FIFOSIZE );
	return ret;
}
// Fügt das übergebene Byte dem Sende Buffer hinzu. Wenn kein Platz in dem Buffer ist, wird solange gewartet, bis ein Byte gesendet wurde.
void fifoTxAdd(char toAdd){
	//warten bis platz in buffer ist.
	while(checkTX())ioToggleLed();
	// Es kann noch zwischen dem Check und der eigentlichen Schreibaktion eine Thread Umschaltung passieren.
	//Füge Daten Hinzu.
	tx_buffer.data[tx_buffer.writePointer] = toAdd;
	tx_buffer.writePointer++;
	if(tx_buffer.writePointer >= FIFOSIZE) tx_buffer.writePointer = 0;
}
// Schreibt das Älteste bzw. als Nächstes zusendende Byte in den übergebenen Speicherbereich. Über den booleschen Rückgabewert kann erkannt werden ob ein Wert vorhanden war.
bool fifoTxGet(char *recive){
	// es sind keine Aktuellen Daten Vorhanden dann False zurück geben.
	  if (tx_buffer.readPointer == tx_buffer.writePointer){
		  return false;
	  }
	  // daten zurückgeben
	  *recive = tx_buffer.data[tx_buffer.readPointer];
	  // die Datenzeiger Manipulieren.
	  tx_buffer.readPointer++;
	  if (tx_buffer.readPointer >= FIFOSIZE)tx_buffer.readPointer = 0;
	  return true;
}
bool checkRX(void){
	bool ret =(rx_buffer.writePointer + 1 == rx_buffer.readPointer ) ||( rx_buffer.readPointer == 0 && rx_buffer.writePointer + 1 == FIFOSIZE );
	return ret;
}
// Fügt das übergebene Byte dem Empfangs Buffer hinzu. Wenn kein Platz in dem Buffer ist, wird solange gewartet, bis ein Byte entnommen wurde.
void fifoRxAdd(char toAdd){
	//warten bis platz in buffer ist.
	while(checkRX());
	// Es kann noch zwischen dem Check und der eigentlichen Schreibaktion eine Thread Umschaltung passieren.
	//Füge Daten Hinzu.
	rx_buffer.data[rx_buffer.writePointer] = toAdd;
	rx_buffer.writePointer++;
	if(rx_buffer.writePointer >= FIFOSIZE) rx_buffer.writePointer = 0;
}
// Schreibt das Älteste bzw. als letztes empfangene Byte in den übergebenen Speicherbereich. Über den booleschen Rückgabewert kann erkannt werden, ob ein Wert vorhanden war.
bool fifoRxGet(char *recive){
	// es sind keine Aktuellen Daten Vorhanden dann False zurück geben.
	  if (rx_buffer.readPointer == rx_buffer.writePointer){
		  return false;
	  }
	  // daten zurückgeben
	  *recive = rx_buffer.data[rx_buffer.readPointer];
	  // die Datenzeiger Manipulieren.
	  rx_buffer.readPointer++;
	  if (rx_buffer.readPointer >= FIFOSIZE)rx_buffer.readPointer = 0;
	  return true;
}
