/*
 * fifo.h
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 *      Hier befindet sich das eigentliche Programm. Es wird auf die verschiedenen Programmteile zugegriffen und alles zu einem funktionierenden und interagierenden Programm vereint. Es stehen mehrere Funktionen zur Verfügung, welche zu unterschiedlichen Zeitpunkten aufgerufen werden.
 */

#ifndef FIFO_H_
#define FIFO_H_
#include <stdbool.h>
#define FIFOSIZE 128
// Initialisierung, weil alle Programmteile über einen solchen verfügen, der Vollständigkeit halber implementiert, bei der fifo wäre dies nicht nötig.
void fifosetup(void);
// Fügt das übergebene Byte dem Sende Buffer hinzu. Wenn kein Platz in dem Buffer ist, wird solange gewartet, bis ein Byte gesendet wurde.
void fifoTxAdd(char toAdd);
// Schreibt das Älteste bzw. als Nächstes zusendende Byte in den übergebenen Speicherbereich. Über den booleschen Rückgabewert kann erkannt werden ob ein Wert vorhanden war.
bool fifoTxGet(char *recive);
// Fügt das übergebene Byte dem Empfangs Buffer hinzu. Wenn kein Platz in dem Buffer ist, wird solange gewartet, bis ein Byte entnommen wurde.
void fifoRxAdd(char toAdd);
// Schreibt das Älteste bzw. als letztes empfangene Byte in den übergebenen Speicherbereich. Über den booleschen Rückgabewert kann erkannt werden, ob ein Wert vorhanden war.
bool fifoRxGet(char *recive);


#endif /* FIFO_H_ */
